FROM docker:latest
RUN apk add --update curl git bash && rm -rf /var/cache/apk/*
CMD ["bash"]